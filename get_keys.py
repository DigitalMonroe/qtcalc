from PyQt5 import QtCore


def text_from_keyboard(interface, event):
    """User keyboard input"""
    if event.key() in (QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter):
        #Enter
        interface.ui.btnEqual.animateClick()
    if event.key() in (QtCore.Qt.Key_Backspace, QtCore.Qt.Key_Delete):
        #Del
        interface.ui.btnDel.animateClick()
    if event.key() == QtCore.Qt.Key_Escape:
        #Esc
        interface.ui.btnEsc.animateClick()

        #0 - 9
    if event.key() == QtCore.Qt.Key_0:
        interface.ui.btn0.animateClick()
    if event.key() == QtCore.Qt.Key_1:
        interface.ui.btn1.animateClick()
    if event.key() == QtCore.Qt.Key_2:
        interface.ui.btn2.animateClick()
    if event.key() == QtCore.Qt.Key_3:
        interface.ui.btn3.animateClick()
    if event.key() == QtCore.Qt.Key_4:
        interface.ui.btn4.animateClick()
    if event.key() == QtCore.Qt.Key_5:
        interface.ui.btn5.animateClick()
    if event.key() == QtCore.Qt.Key_6:
        interface.ui.btn6.animateClick()
    if event.key() == QtCore.Qt.Key_7:
        interface.ui.btn7.animateClick()
    if event.key() == QtCore.Qt.Key_8:
        interface.ui.btn8.animateClick()
    if event.key() == QtCore.Qt.Key_9:
        interface.ui.btn9.animateClick()
        #/////

        #+ - * / ! . %
    if event.key() == QtCore.Qt.Key_Plus:
        interface.ui.btnPlus.animateClick()
    if event.key() == QtCore.Qt.Key_Minus:
        interface.ui.btnDis.animateClick()
    if event.key() == QtCore.Qt.Key_Asterisk:
        interface.ui.btnMul.animateClick()
    if event.key() == QtCore.Qt.Key_Slash:
        interface.ui.btnDiv.animateClick()
    if event.key() == QtCore.Qt.Key_Period:
        interface.ui.btnDot.animateClick()
    if event.key() == QtCore.Qt.Key_Exclam:
        interface.ui.btnFact.animateClick()
    if event.key() == QtCore.Qt.Key_Percent:
        interface.ui.btnPers.animateClick()
